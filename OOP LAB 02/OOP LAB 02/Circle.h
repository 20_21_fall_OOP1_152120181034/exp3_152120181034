/*
* Circle.h
*
*  Created on: 8 Kas 2018
*      Author: ykartal
*/

#ifndef CIRCLE_H_
#define CIRCLE_H_
#define PI 22/7
class Circle {
public:
	Circle(double);
	~Circle();
	void setR(int);
	void setR(double);
	double getR()const;
	double calculateCircumference()const;
	double calculateArea();
	bool compare(Circle);
private:
	double r;
};
#endif /* CIRCLE_H_ */
